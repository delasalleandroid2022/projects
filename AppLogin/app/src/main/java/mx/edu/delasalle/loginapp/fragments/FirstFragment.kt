package mx.edu.delasalle.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentTransaction
import mx.edu.delasalle.loginapp.R


class FirstFragment : Fragment(R.layout.fragment_first) {

    private lateinit var bnGoSecondFragment: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bnGoSecondFragment = view.findViewById(R.id.first_fragment_bn_go_to_second)

        bnGoSecondFragment.setOnClickListener {

            val transaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.replace(R.id.fragment_activity_fv_container, SecondFragment.newInstance("Hugo","Gomez"),
                null)
            transaction.commit()
        }
    }



}